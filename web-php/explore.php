<?php

include_once  'credentials.php';

// header
include_once  'sub_view/header.php';
// body
include_once  'sub_view/explore/topBar.php';
// include 'sub_view/explore/body.php';

//login
if ($isLogin == 0) {
    include_once  'sub_view/index/login.php';
}

include_once  'sub_view/explore/graph.php';
include_once  'sub_view/explore/balance.php';
include_once  'sub_view/explore/belowGraph.php';
include_once  'sub_view/explore/howItWorks.php';
include_once  'sub_view/explore/question.php';
echo '</div>';
include_once  'sub_view/explore/rightSide.php';

// footer
include_once  'sub_view/footer.php';
