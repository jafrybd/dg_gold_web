<?php

session_start();
if (!isset($_SESSION['dg_bangladesh_token'])) {
	header("location: ../index.php");
} else {
	include_once("../third_party_server/server.php");
	$server_obj = new Server();
	$reqData = array();
	$response = $server_obj->get_date("/product/list", $reqData, true);
	$product_list = array();

	if ($response['success'] == 1) {
		$product_list = $response['product_list'];
	}

?>

	<!DOCTYPE html>
	<html lang="en">
	<?php include_once('sub_view/head.php'); ?>

	<body>

		<?php include_once('sub_view/header.php'); ?>
		<!-- / header -->

		<?php include_once('sub_view/nav.php'); ?>
		<!-- / navigation -->

		<?php include_once('sub_view/slider.php'); ?>
		<!-- / body -->

		<div id="body">
			<div class="container">
				<div class="last-products">
					<h2>Last added products</h2>
					<section class="products">
						<?php for ($i = 0; $i < count($product_list); $i++) { ?>
							<article>
								<a href="product-detals.php?id=<?php echo $product_list[$i]['id']; ?>">
									<img src="images/<?php echo  $product_list[$i]['image']; ?>" width="196" height="212" alt="product Image">
								</a>
								<h3> <?php echo  $product_list[$i]['title']; ?></h3>
								<h4><?php echo  $product_list[$i]['karate_weight_gm']; ?> gm</h4>
								<a href="#" onclick="addToCart('<?php echo $product_list[$i]['id']; ?>')" class="btn-add">Add to cart</a>
							</article>
						<?php  } ?>
					</section>
				</div>
				<section class="quick-links">
					<article style="background-image: url(images/2.jpg)">
						<a href="#" class="table">
							<div class="cell">
								<div class="text">
									<h4>Lorem ipsum</h4>
									<hr>
									<h3>Dolor sit amet</h3>
								</div>
							</div>
						</a>
					</article>
					<article class="red" style="background-image: url(images/3.jpg)">
						<a href="#" class="table">
							<div class="cell">
								<div class="text">
									<h4>consequatur</h4>
									<hr>
									<h3>voluptatem</h3>
									<hr>
									<p>Accusantium</p>
								</div>
							</div>
						</a>
					</article>
					<article style="background-image: url(images/4.jpg)">
						<a href="#" class="table">
							<div class="cell">
								<div class="text">
									<h4>culpa qui officia</h4>
									<hr>
									<h3>magnam aliquam</h3>
								</div>
							</div>
						</a>
					</article>
				</section>
			</div>
			<!-- / container -->
		</div>
		<!-- / body -->


		<?php include_once('sub_view/footer.php'); ?>
		<!-- / footer -->

		<?php include_once('sub_view/script.php'); ?>


		<script>
			function addToCart(id) {
				$.ajax({
					url: '../shop_request/add_to_cart.php',
					type: 'POST',
					dataType: 'html',
					data: {
						product_id: id
					},
					success: function(response) {
						if (response == 1990) {
							window.location = '../logout.php'
						} else {
							alert("Product Added to Cart.")
						}
					},
					error: function(xhr, textStatus, errorThrown) {
						console.log("Fail");
					}

				});

			}
		</script>

	</body>

	</html>

<?php } ?>