<div class="col l12" style="min-height: 450px;">
    <div class="valign-wrapper vspace-between">
        <div class="gdh28Head">
            <div class="valign-wrapper gdh28Container">
                <div class="gdh28ImgDiv">
                    <div class="lazyload-wrapper"><img class="gdh28AugmontLogo" src="//assets-netstorage.groww.in/website-assets/prod/1.5.9/build/client/images/logo-augmont.cb7c8652.png" alt="Augmont" width="60" height="60"></div>
                </div>
                <div class="gdh28ContentDiv">
                    <div class="gdh28AugmontTxt"></div>
                    <div class="gdh28DigitalGoldTxt">Digital Gold Bangladesh</div>
                </div>
            </div>
            <div class="valign-wrapper" style="margin-top: 10px; margin-left: 70px;">
                <div class="gdh28TagDiv">Gold - 22K 99.90%</div>
            </div>
        </div>
        <div class="gdh28RightSide">
            <div class="gdh28Returns"><span>-7.64</span>%</div>
            <div class="gdh28Range">6M Return</div>
        </div>
    </div>
    <div class="gold1GraphDiv">
        <div id="goldGraph" class="onMount-appear-done onMount-enter-done">
            <div style="display: block;">
                <div id="gold-graph" style="height: 250px;">
                    <div class="onMount-appear-done onMount-enter-done" data-highcharts-chart="105">
                        <div id="highcharts-ss7yjda-30248" style="position: relative; overflow: hidden; width: 710px; height: 250px; text-align: left; line-height: normal; z-index: 0; left: 0px; top: 0px;" dir="ltr" class="highcharts-container ">
                            <!--  -->
                            <div id="chartContainer" style="height: 370px; width: 100%;"></div>
                            <script src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>
                            <script src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
                            <!--  -->
                            <div class="highcharts-tooltip" style="position: absolute; left: 0px; top: 0px; opacity: 0; visibility: visible;">
                                <div class="highcharts-label highcharts-tooltip-box highcharts-color-0" style="position: absolute; left: 515px; top: 122px; opacity: 1; visibility: inherit;"><span style="font-family: Lucida Grande, Lucida Sans Unicode, Arial, Helvetica, sans-serif; font-size: 12px; position: absolute; white-space: nowrap; margin-left: 0px; margin-top: 0px; left: 8px; top: 8px; color: rgb(51, 51, 51); cursor: default;" data-z-index="1">
                                        <div><span style="font-size: 12px; font-weight:700;">4801.09 TK</span><span style="font-size: 12px"> | 01 May 2021</span></div>
                                    </span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="valign-wrapper gold1Para13 gold1YearWrapper">
                <div class="valign-wrapper" style="position: relative;">
                    <div class="gold1Padding1020"><span>6M</span></div>
                    <div class="gold1Padding1020"><span>1Y</span></div>
                    <div class="gold1Padding1020"><span>3Y</span></div>
                    <div class="gold1Padding1020"><span>5Y</span></div>
                    <div class="gold1GreenLine gold1FirstGreenLine"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- script -->
<script>
    window.onload = function() {

        var dataPoints = [];

        var chart = new CanvasJS.Chart("chartContainer", {
            theme: "light2",
            
            data: [{
                type: "line",
                dataPoints: dataPoints
            }]
        });
        updateData();

        // Initial Values
        var xValue = 0;
        var yValue = 10;
        var newDataCount = 6;

        function addData(data) {
            if (newDataCount != 1) {
                $.each(data, function(key, value) {
                    dataPoints.push({
                        x: value[0],
                        y: parseInt(value[1])
                    });
                    xValue++;
                    yValue = parseInt(value[1]);
                });
            } else {
                //dataPoints.shift();
                dataPoints.push({
                    x: data[0][0],
                    y: parseInt(data[0][1])
                });
                xValue++;
                yValue = parseInt(data[0][1]);
            }

            newDataCount = 1;
            chart.render();
            setTimeout(updateData, 1500);
        }

        function updateData() {
            $.getJSON("https://canvasjs.com/services/data/datapoints.php?xstart=" + xValue + "&ystart=" + yValue + "&length=" + newDataCount + "type=json", addData);
        }

    }
</script>