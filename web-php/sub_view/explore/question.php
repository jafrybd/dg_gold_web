<div class="col l12">
    <div id="abtGoldFaq">
        <h3 class="componentsMainHeading gf11Heading">Frequently Asked Questions</h3>
        <div class="fs567FaqWrapper">
            <div class="fs567FaqTopicDiv">
                <h3 class="fs567Header">1. About Digital Gold</h3>
                <div class="fs567QuestionDiv">
                    <div class="cur-po ">
                        <div class="valign-wrapper vspace-between acc11HeaderMain fs567QuestionTxt">
                            <div class="acc11Title ">
                                <div class="fs567Label"> What is Digital Gold?</div>
                            </div><a class="accordion-toggle" data-toggle="collapse" data-parent="#leftMenu" href="#tab-1"><i class="material-icons is31Default acc1Icon" style="width: 24px; height: 24px;">keyboard_arrow_down</i></a>
                        </div>
                        <div>
                            <div id="tab-1" class="panel-collapse collapse">
                                <div class="fs567Desc">
                                    <div>Digital Gold is a convenient and cost-effective way of purchasing gold online. You can buy, sell, and accumulate gold of 99.90% purity in fractions anytime — all you need is your laptop/desktop and access to the internet.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fs567QuestionDiv">
                    <div class="cur-po ">
                        <div class="valign-wrapper vspace-between acc11HeaderMain fs567QuestionTxt">
                            <div class="acc11Title ">
                                <div class="fs567Label"> Where does Digital Gold Bangladesh get the gold from?</div>
                            </div><a class="accordion-toggle" data-toggle="collapse" data-parent="#leftMenu" href="#tab-2"><i class="material-icons is31Default acc1Icon" style="width: 24px; height: 24px;">keyboard_arrow_down</i></a>
                        </div>
                        <div>
                            <div id="tab-2" class="panel-collapse collapse">
                                <div class="fs567Desc">
                                    <div>To provide you pure and top-quality bullion, Digital Gold Bangladesh has partnered with Augmont Gold Pvt. Ltd.—an integrated precious metals management company. All purchase/sell transactions of Digital Gold will be directly with Augmont Gold Pvt Ltd. Augmont also has businesses in gold refining and manufacturing of tamper-proof packaged jewellery.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fs567QuestionDiv">
                    <div class="cur-po ">
                        <div class="valign-wrapper vspace-between acc11HeaderMain fs567QuestionTxt">
                            <div class="acc11Title ">
                                <div class="fs567Label"> What is the Gold Locker?</div>
                            </div><a class="accordion-toggle" data-toggle="collapse" data-parent="#leftMenu" href="#tab-3"><i class="material-icons is31Default acc1Icon" style="width: 24px; height: 24px;">keyboard_arrow_down</i></a>

                        </div>
                        <div>
                            <div id="tab-3" class="panel-collapse collapse">
                                <div class="fs567Desc">
                                    <div>Gold Locker is a digital version of your holdings, wherein you can view your Digital Gold transactions.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fs567QuestionDiv">
                    <div class="cur-po ">
                        <div class="valign-wrapper vspace-between acc11HeaderMain fs567QuestionTxt">
                            <div class="acc11Title ">
                                <div class="fs567Label"> What is purity of the gold offered by Augmont?</div>
                            </div><a class="accordion-toggle" data-toggle="collapse" data-parent="#leftMenu" href="#tab-4"><i class="material-icons is31Default acc1Icon" style="width: 24px; height: 24px;">keyboard_arrow_down</i></a>

                        </div>
                        <div>
                            <div id="tab-4" class="panel-collapse collapse">
                                <div class="fs567Desc">
                                    <div>Augmont offers 22-Karat gold of 999 purity (99.90% pure).</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fs567QuestionDiv">
                    <div class="cur-po ">
                        <div class="valign-wrapper vspace-between acc11HeaderMain fs567QuestionTxt">
                            <div class="acc11Title ">
                                <div class="fs567Label"> What are the charges applicable?</div>
                            </div><a class="accordion-toggle" data-toggle="collapse" data-parent="#leftMenu" href="#tab-5"><i class="material-icons is31Default acc1Icon" style="width: 24px; height: 24px;">keyboard_arrow_down</i></a>

                        </div>
                        <div>
                            <div id="tab-5" class="panel-collapse collapse">
                                <div class="fs567Desc">
                                    <div>
                                        <div class="gf11Strong">Charges during purchase: </div>The live price is quoted on the basis of wholesale prices in the spot market. The gold rate excludes taxes, making charges and delivery charges. 3% GST is applicable while buying gold .<br>
                                        <div class="gf11Strong">Charges during sale: </div>When you are selling your gold back to Augmont, you can sell it back at the live price on a real time basis. There are no additional charges applicable.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fs567QuestionDiv">
                    <div class="cur-po ">
                        <div class="valign-wrapper vspace-between acc11HeaderMain fs567QuestionTxt">
                            <div class="acc11Title ">
                                <div class="fs567Label"> What are the taxes involved?</div>
                            </div><a class="accordion-toggle" data-toggle="collapse" data-parent="#leftMenu" href="#tab-6"><i class="material-icons is31Default acc1Icon" style="width: 24px; height: 24px;">keyboard_arrow_down</i></a>

                        </div>
                        <div>
                            <div id="tab-6" class="panel-collapse collapse">
                                <div class="fs567Desc">
                                    <div>The gold rates shown on Digital Gold Bangladesh at the time of purchasing/selling is exclusive of taxes. However, tax on income from sale of gold is applicable.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fs567FaqTopicDiv">
                <h3 class="fs567Header">2. Eligibility Criteria</h3>
                <div class="fs567QuestionDiv">
                    <div class="cur-po ">
                        <div class="valign-wrapper vspace-between acc11HeaderMain fs567QuestionTxt">
                            <div class="acc11Title ">
                                <div class="fs567Label"> What is the eligibility criteria for buying Digital Gold on Digital Gold Bangladesh?</div>
                            </div><a class="accordion-toggle" data-toggle="collapse" data-parent="#leftMenu" href="#tab-7"><i class="material-icons is31Default acc1Icon" style="width: 24px; height: 24px;">keyboard_arrow_down</i></a>

                        </div>
                        <div>
                            <div id="tab-7" class="panel-collapse collapse">
                                <div class="fs567Desc">
                                    <div>Any resident Indian with verified KYC will be eligible for purchasing Digital Gold on Digital Gold Bangladesh.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fs567QuestionDiv">
                    <div class="cur-po ">
                        <div class="valign-wrapper vspace-between acc11HeaderMain fs567QuestionTxt">
                            <div class="acc11Title ">
                                <div class="fs567Label"> Do I need to complete the KYC to buy Digital Gold on Digital Gold Bangladesh?</div>
                            </div><a class="accordion-toggle" data-toggle="collapse" data-parent="#leftMenu" href="#tab-8"><i class="material-icons is31Default acc1Icon" style="width: 24px; height: 24px;">keyboard_arrow_down</i></a>

                        </div>
                        <div>
                            <div id="tab-8" class="panel-collapse collapse">
                                <div class="fs567Desc">
                                    <div>Yes. To be eligibile for purchasing Digital Gold on Digital Gold Bangladesh, you need to be a resident Indian with verified KYC.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fs567FaqTopicDiv">
                <h3 class="fs567Header">3. Buying Digital Gold</h3>
                <div class="fs567QuestionDiv">
                    <div class="cur-po ">
                        <div class="valign-wrapper vspace-between acc11HeaderMain fs567QuestionTxt">
                            <div class="acc11Title ">
                                <div class="fs567Label"> How do I buy Digital Gold on Digital Gold Bangladesh?</div>
                            </div><a class="accordion-toggle" data-toggle="collapse" data-parent="#leftMenu" href="#tab-9"><i class="material-icons is31Default acc1Icon" style="width: 24px; height: 24px;">keyboard_arrow_down</i></a>

                        </div>
                        <div>
                            <div id="tab-9" class="panel-collapse collapse">

                                <div class="fs567Desc">
                                    <div>
                                        <div class="gf11Strong">1. Enter the amount:</div> You can input your desired purchase amount in rupees or grams of gold.<br>
                                        <div class="gf11Strong">2. Buy in one-click:</div> Review your order and place order using UPI/Netbanking within the 5-minute price window.<br>
                                        <div class="gf11Strong">3. Secured and insured:</div> Your gold locker will be updated, where you can view your purchased Digital Gold holdings.<br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fs567QuestionDiv">
                    <div class="cur-po ">
                        <div class="valign-wrapper vspace-between acc11HeaderMain fs567QuestionTxt">
                            <div class="acc11Title ">
                                <div class="fs567Label"> What is live price?</div>
                            </div><a class="accordion-toggle" data-toggle="collapse" data-parent="#leftMenu" href="#tab-10"><i class="material-icons is31Default acc1Icon" style="width: 24px; height: 24px;">keyboard_arrow_down</i></a>

                        </div>
                        <div>
                            <div id="tab-10" class="panel-collapse collapse">

                                <div class="fs567Desc">
                                    <div>The live price shown is the current market price of gold, exclusive of taxes. This price may change throughout the day. However, once you begin a purchase/sell transaction, the live price shown will be locked in for 5 minutes.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fs567QuestionDiv">
                    <div class="cur-po ">
                        <div class="valign-wrapper vspace-between acc11HeaderMain fs567QuestionTxt">
                            <div class="acc11Title ">
                                <div class="fs567Label"> Is there any minimum order quantity or price to buy Digital Gold?</div>
                            </div><a class="accordion-toggle" data-toggle="collapse" data-parent="#leftMenu" href="#tab-11"><i class="material-icons is31Default acc1Icon" style="width: 24px; height: 24px;">keyboard_arrow_down</i></a>

                        </div>
                        <div>
                            <div id="tab-11" class="panel-collapse collapse">

                                <div class="fs567Desc">
                                    <div>You can buy Digital Gold on Digital Gold Bangladesh for as low as 50 TK.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fs567QuestionDiv">
                    <div class="cur-po ">
                        <div class="valign-wrapper vspace-between acc11HeaderMain fs567QuestionTxt">
                            <div class="acc11Title ">
                                <div class="fs567Label"> Is there any maximum order quantity of Digital Gold that you can buy?</div>
                            </div><a class="accordion-toggle" data-toggle="collapse" data-parent="#leftMenu" href="#tab-12"><i class="material-icons is31Default acc1Icon" style="width: 24px; height: 24px;">keyboard_arrow_down</i></a>

                        </div>
                        <div>
                            <div id="tab-12" class="panel-collapse collapse">

                                <div class="fs567Desc">
                                    <div>For the first order, you can buy gold worth Rs 1.7 lakh. For all subsequent purchases, there is no such restriction on the amount of gold that you can buy.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fs567QuestionDiv">
                    <div class="cur-po ">
                        <div class="valign-wrapper vspace-between acc11HeaderMain fs567QuestionTxt">
                            <div class="acc11Title ">
                                <div class="fs567Label"> Can I cancel my Digital Gold purchase?</div>
                            </div><a class="accordion-toggle" data-toggle="collapse" data-parent="#leftMenu" href="#tab-13"><i class="material-icons is31Default acc1Icon" style="width: 24px; height: 24px;">keyboard_arrow_down</i></a>

                        </div>
                        <div>
                            <div id="tab-13" class="panel-collapse collapse">

                                <div class="fs567Desc">
                                    <div>Once an order has been placed successfully, it cannot be cancelled.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fs567QuestionDiv">
                    <div class="cur-po ">
                        <div class="valign-wrapper vspace-between acc11HeaderMain fs567QuestionTxt">
                            <div class="acc11Title ">
                                <div class="fs567Label"> Will I get an invoice for a Digital Gold purchase order?</div>
                            </div><a class="accordion-toggle" data-toggle="collapse" data-parent="#leftMenu" href="#tab-14"><i class="material-icons is31Default acc1Icon" style="width: 24px; height: 24px;">keyboard_arrow_down</i></a>

                        </div>
                        <div>
                            <div id="tab-14" class="panel-collapse collapse">

                                <div class="fs567Desc">
                                    <div>Yes, you will receive an email confirmation with an invoice attached on every Digital Gold purchase order.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fs567QuestionDiv">
                    <div class="cur-po ">
                        <div class="valign-wrapper vspace-between acc11HeaderMain fs567QuestionTxt">
                            <div class="acc11Title ">
                                <div class="fs567Label"> How can I pay for my Digital Gold purchase?</div>
                            </div><a class="accordion-toggle" data-toggle="collapse" data-parent="#leftMenu" href="#tab-15"><i class="material-icons is31Default acc1Icon" style="width: 24px; height: 24px;">keyboard_arrow_down</i></a>

                        </div>
                        <div>
                            <div id="tab-15" class="panel-collapse collapse">

                                <div class="fs567Desc">
                                    <div>You can pay easily using UPI/Netbanking to purchase Digital gold on Digital Gold Bangladesh.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fs567FaqTopicDiv">
                <h3 class="fs567Header">4. Storage and Insurance</h3>
                <div class="fs567QuestionDiv">
                    <div class="cur-po ">
                        <div class="valign-wrapper vspace-between acc11HeaderMain fs567QuestionTxt">
                            <div class="acc11Title ">
                                <div class="fs567Label"> How is the storage of physical gold taking place?</div>
                            </div><a class="accordion-toggle" data-toggle="collapse" data-parent="#leftMenu" href="#tab-16"><i class="material-icons is31Default acc1Icon" style="width: 24px; height: 24px;">keyboard_arrow_down</i></a>

                        </div>
                        <div>
                            <div id="tab-16" class="panel-collapse collapse">

                                <div class="fs567Desc">
                                    <div>The physical gold is stored by Augmont in 100% secured &nbsp;<a href="https://sequelglobal.com/" class="" rel="nofollow" target="_blank">Sequel vaults</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fs567QuestionDiv">
                    <div class="cur-po ">
                        <div class="valign-wrapper vspace-between acc11HeaderMain fs567QuestionTxt">
                            <div class="acc11Title ">
                                <div class="fs567Label"> Is the gold insured?</div>
                            </div><a class="accordion-toggle" data-toggle="collapse" data-parent="#leftMenu" href="#tab-17"><i class="material-icons is31Default acc1Icon" style="width: 24px; height: 24px;">keyboard_arrow_down</i></a>

                        </div>
                        <div>
                            <div id="tab-17" class="panel-collapse collapse">

                                <div class="fs567Desc">
                                    <div>Yes, the physical gold is secured in Sequel vaults and is insured. When you buy gold, there’s a small margin added to it for insurance as well as other charges like payment gateway, trustee and custodian charges etc.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fs567QuestionDiv">
                    <div class="cur-po ">
                        <div class="valign-wrapper vspace-between acc11HeaderMain fs567QuestionTxt">
                            <div class="acc11Title ">
                                <div class="fs567Label"> What will happen to my gold if Augmont/Bullion India goes into liquidation?</div>
                            </div><a class="accordion-toggle" data-toggle="collapse" data-parent="#leftMenu" href="#tab-18"><i class="material-icons is31Default acc1Icon" style="width: 24px; height: 24px;">keyboard_arrow_down</i></a>

                        </div>
                        <div>
                            <div id="tab-18" class="panel-collapse collapse">

                                <div class="fs567Desc">
                                    <div>Your gold assets are independent of the other assets of Bullion India. IDBI Trusteeship Services Pvt. Ltd. (appointed by Bullion India) acts as an independent trustee, who will ensure that the interests of the customers are protected. The trustee has sole and exclusive charge of the customer's gold held in the vault. Thus, in the event of any adverse effect to Bullion India, it should not effect the customer's gold assets.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fs567FaqTopicDiv">
                <h3 class="fs567Header">5. Selling Digital Gold</h3>
                <div class="fs567QuestionDiv">
                    <div class="cur-po ">
                        <div class="valign-wrapper vspace-between acc11HeaderMain fs567QuestionTxt">
                            <div class="acc11Title ">
                                <div class="fs567Label"> How do I sell Digital Gold purchased on Digital Gold Bangladesh?</div>
                            </div><a class="accordion-toggle" data-toggle="collapse" data-parent="#leftMenu" href="#tab-19"><i class="material-icons is31Default acc1Icon" style="width: 24px; height: 24px;">keyboard_arrow_down</i></a>

                        </div>
                        <div>
                            <div id="tab-19" class="panel-collapse collapse">

                                <div class="fs567Desc">
                                    <div>Digital Gold purchased on Digital Gold Bangladesh can be sold back to Augmont anytime after the 2-day holding period from the date of purchase.<br>
                                        <div class="gf11Strong">1. Click on the sell tab:</div> Enter units to sell at the current selling price shown.<br>
                                        <div class="gf11Strong">2. Sell in one-click:</div> Review and place your sell request within the 5-minute price window.<br>
                                        <div class="gf11Strong">3. Request confirmed:</div> Your gold locker will be updated, where you can view your Digital Gold balance.<br>You will receive the amount in your chosen bank account within 2 working days.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fs567QuestionDiv">
                    <div class="cur-po ">
                        <div class="valign-wrapper vspace-between acc11HeaderMain fs567QuestionTxt">
                            <div class="acc11Title ">
                                <div class="fs567Label"> Why is there a difference in the buy and sell price of Digital Gold?</div>
                            </div><a class="accordion-toggle" data-toggle="collapse" data-parent="#leftMenu" href="#tab-20"><i class="material-icons is31Default acc1Icon" style="width: 24px; height: 24px;">keyboard_arrow_down</i></a>

                        </div>
                        <div>
                            <div id="tab-20" class="panel-collapse collapse">

                                <div class="fs567Desc">
                                    <div>Gold like any other commodity which is tradable has a buy-sell spread. The spread changes on the basis of various factors including price volatility, demand, supply and other external factors. This is the reason why we see a difference of 8-10% in buying and selling of gold coins. This difference is even higher for jewellery on account of making charges.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fs567QuestionDiv">
                    <div class="cur-po ">
                        <div class="valign-wrapper vspace-between acc11HeaderMain fs567QuestionTxt">
                            <div class="acc11Title ">
                                <div class="fs567Label"> How soon can I sell the Digital Gold bought on Digital Gold Bangladesh?</div>
                            </div><a class="accordion-toggle" data-toggle="collapse" data-parent="#leftMenu" href="#tab-21"><i class="material-icons is31Default acc1Icon" style="width: 24px; height: 24px;">keyboard_arrow_down</i></a>

                        </div>
                        <div>
                            <div id="tab-21" class="panel-collapse collapse">

                                <div class="fs567Desc">
                                    <div>You will be able to sell the Gold bought on Digital Gold Bangladesh after 2 days from the date of purchase.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fs567QuestionDiv">
                    <div class="cur-po ">
                        <div class="valign-wrapper vspace-between acc11HeaderMain fs567QuestionTxt">
                            <div class="acc11Title ">
                                <div class="fs567Label"> Where is the money settled when I sell Digital Gold on Digital Gold Bangladesh?</div>
                            </div><a class="accordion-toggle" data-toggle="collapse" data-parent="#leftMenu" href="#tab-22"><i class="material-icons is31Default acc1Icon" style="width: 24px; height: 24px;">keyboard_arrow_down</i></a>

                        </div>
                        <div>
                            <div id="tab-22" class="panel-collapse collapse">

                                <div class="fs567Desc">
                                    <div>On selling Digital Gold, the money will be settled and transferred to your chosen bank account within 2 working days.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fs567FaqTopicDiv">
                <h3 class="fs567Header">6. Order Related</h3>
                <div class="fs567QuestionDiv">
                    <div class="cur-po ">
                        <div class="valign-wrapper vspace-between acc11HeaderMain fs567QuestionTxt">
                            <div class="acc11Title ">
                                <div class="fs567Label"> What if my payment amount on sale of the Gold has not been credited to my account?</div>
                            </div><a class="accordion-toggle" data-toggle="collapse" data-parent="#leftMenu" href="#tab-23"><i class="material-icons is31Default acc1Icon" style="width: 24px; height: 24px;">keyboard_arrow_down</i></a>

                        </div>
                        <div>
                            <div id="tab-23" class="panel-collapse collapse">

                                <div class="fs567Desc">
                                    <div>You can choose the bank to receive the amount from the list of verified bank accounts linked on Digital Gold Bangladesh at the time of placing the sell order. Money will be credited to your account within 2 working days. If you haven't received the money in your bank account, reach out to us at<div class="gf11Strong">support@Digital Gold Bangladesh.in</div> with the transaction id and we will take it up with Augmont</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fs567QuestionDiv">
                    <div class="cur-po ">
                        <div class="valign-wrapper vspace-between acc11HeaderMain fs567QuestionTxt">
                            <div class="acc11Title ">
                                <div class="fs567Label"> Why did my sell order fail?</div>
                            </div><a class="accordion-toggle" data-toggle="collapse" data-parent="#leftMenu" href="#tab-24"><i class="material-icons is31Default acc1Icon" style="width: 24px; height: 24px;">keyboard_arrow_down</i></a>

                        </div>
                        <div>
                            <div id="tab-24" class="panel-collapse collapse">

                                <div class="fs567Desc">
                                    <div>In the rare case of a technical glitch, if Augmont cancels your sell order, the quantity offered will be credited back to your gold account.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fs567FaqTopicDiv">
                <h3 class="fs567Header">7. Delivery of gold</h3>
                <div class="fs567QuestionDiv">
                    <div class="cur-po ">
                        <div class="valign-wrapper vspace-between acc11HeaderMain fs567QuestionTxt">
                            <div class="acc11Title ">
                                <div class="fs567Label"> Can I request delivery of my gold?</div>
                            </div><a class="accordion-toggle" data-toggle="collapse" data-parent="#leftMenu" href="#tab-25"><i class="material-icons is31Default acc1Icon" style="width: 24px; height: 24px;">keyboard_arrow_down</i></a>

                        </div>
                        <div>
                            <div id="tab-25" class="panel-collapse collapse">

                                <div class="fs567Desc">
                                    <div>We are working on making doorstep delivery for your digital gold. This feature will be launched very soon</div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>