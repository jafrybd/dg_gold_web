<div id="footer" style="margin-top: 100px;">
    <div class="row">
        <div class="col-sm-12 fm77Cover1">
            <div class="container web-align">
                <div class="row" style="padding-top: 30px;">
                    <div class="col-sm-3">
                        <div class="valign-wrapper cur-po pos-rel" style="min-width: 150px; min-height: 79px;" itemscope="true" itemtype="http://schema.org/Brand"><img class="" src="./images/logo2.png" alt="Digital Gold Bangladesh Logo" itemprop="logo" width="350" height="150"></div>
                        <p class="fm77Line1">No.11, 2nd floor, 80 FT Road</p>
                        <p class="fm77Line2">4th Block, S.T Bed</p>
                        <p class="fm77Line2">Koramangala, Bengaluru – 560034</p>
                        <div class="fm77Contact">
                            <p class="fm77Line2" style="line-height: 10px; color:var(--constantWhite);"><u>Contact Us</u></p>
                        </div>
                        <div class="valign-wrapper" style="margin-top: 25px;">
                            <div class="fm77IconsMargin"><a href="" class="col l12" target="_blank" rel="nofollow">
                                    <div class="lazyload-wrapper"><img class="" src="https://assets-netstorage.groww.in/website-assets/prod/1.5.8/build/client/images/fb_icon_groww.1c94e937.svg" alt="Digital Gold Bangladesh FB Page" width="40" height="30"></div>
                                </a></div>
                            <div class="fm77IconsMargin"><a href="" class="col l12" target="_blank" rel="nofollow">
                                    <div class="lazyload-wrapper"><img class="" src="https://assets-netstorage.groww.in/website-assets/prod/1.5.8/build/client/images/twitter_icon_groww.4cb988f6.svg" alt="Digital Gold Bangladesh Twitter Page" width="40" height="30"></div>
                                </a></div>
                            <div class="fm77IconsMargin"><a href="" class="col l12" target="_blank" rel="nofollow">
                                    <div class="lazyload-wrapper"><img class="" src="https://assets-netstorage.groww.in/website-assets/prod/1.5.8/build/client/images/yt_icon_groww.ec96b677.svg" alt="Youtube Digital Gold Bangladesh" width="40" height="30"></div>
                                </a></div>
                            <div class="fm77IconsMargin"><a href="" class="col l12" target="_blank" rel="nofollow">
                                    <div class="lazyload-wrapper"><img class="" src="https://assets-netstorage.groww.in/website-assets/prod/1.5.8/build/client/images/instagram_icon_groww.0454c1a2.svg" alt="Digital Gold Bangladesh Instagram Page" width="40" height="30"></div>
                                </a></div>
                            <div class="fm77IconsMargin"><a href="" class="col l12" target="_blank" rel="nofollow">
                                    <div class="lazyload-wrapper"><img class="" src="https://assets-netstorage.groww.in/website-assets/prod/1.5.8/build/client/images/linkedin_icon_groww.b15f8240.svg" alt="Digital Gold Bangladesh Linkedin Page" width="40" height="30"></div>
                                </a></div>
                            <div class="fm77IconsMargin"><a href="" class="col l12" target="_blank" rel="nofollow">
                                    <div class="lazyload-wrapper"><img class="" src="https://assets-netstorage.groww.in/website-assets/prod/1.5.8/build/client/images/telegram_icon_groww.f6524497.svg" alt="Digital Gold Bangladesh Telegram Page" width="40" height="30"></div>
                                </a></div>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="col l4" style="margin-top: 17px;">
                            <div class="col l12 fm77FooterHeading">Digital Gold Bangladesh</div><a class="col l12 fm77FooterLink" href="/about-us">About Us</a><a class="col l12 fm77FooterLink" href="/mutual-funds/amc">AMC Mutual Funds</a><a class="col l12 fm77FooterLink" href="/pricing/">Pricing</a><a class="col l12 fm77FooterLink" href="/questions">Digital Gold Bangladesh Q&amp;A</a><a href="https://groww.in/blog/" class="col l12 fm77FooterLink" target="_blank">Blog</a><a href="https://groww.in/p/press/" class="col l12 fm77FooterLink" target="_blank">Media &amp; Press</a><a class="col l12 fm77FooterLink" href="/help">Help and Support</a>
                        </div>
                        <div class="col l4" style="margin-top: 17px;">
                            <div class="col l12 fm77FooterHeading">EXPLORE</div><a class="col l12 fm77FooterLink" href="/mutual-funds/filter">Mutual Funds</a><a class="col l12 fm77FooterLink" href="/stocks/filter">Stocks</a><a class="col l12 fm77FooterLink" href="/nfo">NFO</a><a class="col l12 fm77FooterLink" href="/ipo">IPO</a><a class="col l12 fm77FooterLink" href="/us-stocks/filter">US Stocks</a><a class="col l12 fm77FooterLink" href="/explore/deposits">Fixed Deposit</a><a class="col l12 fm77FooterLink" href="/explore/gold">Gold</a>
                        </div>
                        <div class="col l4" style="margin-top: 17px;">
                            <div class="col l12 fm77FooterHeading">RESOURCES</div><a href="" class="col l12 fm77FooterLink" target="_blank">Mutual Fund Beginners</a><a href="https://groww.in/blog/how-to-invest-in-share-market/" class="col l12 fm77FooterLink" target="_blank">Learn Stock Market</a><a href="https://groww.in/calculators/" class="col l12 fm77FooterLink" target="_blank">Calculators</a><a href="https://groww.in/p/" class="col l12 fm77FooterLink" target="_blank">Glossary</a><a class="col l12 fm77FooterLink" href="/mutual-funds/compare">Compare Funds</a><a class="col l12 fm77FooterLink" href="/user/switch">Switch to Digital Gold Bangladesh</a><a href="https://groww.in/p/download-forms/" class="col l12 fm77FooterLink" target="_blank">Download Forms</a>
                        </div>
                    </div>
                </div>
                <div class="col l12 fm77FooterLine"></div>
                <div class="col l12 fm77CopyRightDiv">
                    <div class="col l6 fm77CompanyName valign-wrapper">ⓒ&nbsp;2021. All rights reserved, Built with <span class="fm77HeartImg">♥</span>in Bangladesh</div>
                    <div class="col l6 valign-wrapper fm77GPlayDiv">
                        <div class="fm77PlayStoreDiv clearfix">
                            <div><a href="#" class="" rel="nofollow">
                                    <div class="lazyload-wrapper"><img class="" src="https://assets-netstorage.groww.in/website-assets/prod/1.5.8/build/client/images/app-store-logo.060773ea.svg" alt="Download Digital Gold Bangladesh App on apple store" width="147" height="45"></div>
                                </a></div>
                        </div>
                        <div class="fm77PlayStoreDiv clearfix">
                            <div><a href="#" class="" rel="nofollow">
                                    <div class="lazyload-wrapper"><img class="" src="https://assets-netstorage.groww.in/website-assets/prod/1.5.8/build/client/images/google-play-badge.0547a72f.svg" alt="Download Digital Gold Bangladesh App on play store" width="147" height="45"></div>
                                </a></div>
                            <!-- </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>
</div>
</div>

<script src="https://assets-netstorage.groww.in/website-assets/prod/1.5.8/build/client/js/webpackManifest.7e9f55ae.js"></script>
<script src="https://assets-netstorage.groww.in/website-assets/prod/1.5.8/build/client/js/vendor.18b01aa4.js"></script>
<script src="https://assets-netstorage.groww.in/website-assets/prod/1.5.8/build/client/js/GoldLandingPage.d7ccb3e3.js"></script>


<script id='_webengage_script_tag' type="text/javascript" async defer>
    function downloadJSAtOnload() {
        try {

            (function(w, d, s, l, i) {
                w[l] = w[l] || [];
                w[l].push({
                    'gtm.start': new Date().getTime(),
                    event: 'gtm.js'
                });
                var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s),
                    dl = l != 'dataLayer' ? '&l=' + l : '';
                j.async = !0;
                j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                f.parentNode.insertBefore(j, f)
            })(window, document, 'script', 'dataLayer', 'GTM-T7VTWPR')

            var webengage;
            ! function(w, e, b, n, g) {
                function o(e, t) {
                    e[t[t.length - 1]] = function() {
                        r.__queue.push([t.join("."), arguments])
                    }
                }
                var i, s, r = w[b],
                    z = " ",
                    l = "init options track screen onReady".split(z),
                    a = "feedback survey notification".split(z),
                    c = "options render clear abort".split(z),
                    p = "Open Close Submit Complete View Click".split(z),
                    u = "identify login logout setAttribute".split(z);
                if (!r || !r.__v) {
                    for (w[b] = r = {
                            __queue: [],
                            is_spa: 1,
                            __v: "6.0",
                            user: {}
                        }, i = 0; i < l.length; i++) o(r, [l[i]]);
                    for (i = 0; i < a.length; i++) {
                        for (r[a[i]] = {}, s = 0; s < c.length; s++) o(r[a[i]], [a[i], c[s]]);
                        for (s = 0; s < p.length; s++) o(r[a[i]], [a[i], "on" + p[s]])
                    }
                    for (i = 0; i < u.length; i++) o(r.user, ["user", u[i]]);
                    setTimeout(function() {
                        var f = e.createElement("script"),
                            d = e.getElementById("_webengage_script_tag");
                        f.type = "text/javascript", f.async = !0, f.src = ("https:" == e.location.protocol ? "https://ssl.widgets.webengage.com" : "http://cdn.widgets.webengage.com") + "/js/webengage-min-v-6.0.js", d.parentNode.insertBefore(f, d)
                    })
                }
            }(window, document, "webengage");
            window.webengage.init("d3a4ab31");

        } catch (err) {
            console.log("Ext JS error", err);
        }
    }
    if (window.addEventListener) window.addEventListener("load", downloadJSAtOnload, !1);
    else if (window.attachEvent) window.attachEvent("onload", downloadJSAtOnload);
    else window.onload = downloadJSAtOnload;
</script>
</body>

</html>