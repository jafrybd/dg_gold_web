<div class="container web-align">
    <section id="goldPartner">
        <div class="gp76partnerWrapper">
            <div class="gp76partnerContent">
                <div class="lazyload-wrapper"><img class="gp76partnerIcon" src="./shop/images/logo.png" alt="augmont logo" width="54" height="54"></div><span class="gp76partnerText">PARTNERSHIP</span>
                <div class="gp76heading">Digital Gold Bangladesh has partnered with Amin jewellers.</div>
                <div class="gp76para">An integrated precious metals management company.</div>
            </div>
        </div>
    </section>
</div>