<!-- Login Modal -->

<div id="myModal" class="modal" style="animation-duration: 300ms;" class="rodal rodal-fade-enter" tabindex="-1">
    <div class='modal-content' style="width: 900px; height: auto; animation-duration: 300ms;" class="rodal-dialog rodal-zoom-enter">
        <div class="child-wrapper lpw107LoginPopupWrapper">
            <div class="row" style="height: 475px!important;">
                <div class="col l12 valign-wrapper lc723LoginMainDiv">
                    <div class="lc723LeftDiv">
                        <div class="lc723LeftWrapper"><img class="lls723LoginImg" src="//assets-netstorage.groww.in/website-assets/prod/1.5.9/build/client/images/patternImg.0edf5760.svg" alt="pattern groww" width="450" height="475">
                            <div class="lls723LeftTextWrapper">
                                <div class="lls723LeftTopText">Simple, Free</div>
                                <div class="lls723LeftTopText">Investing.</div>
                                <div class="lls723LeftDashAnim"></div>
                                <div class="lls723LeftBottomText">Direct Mutual Funds</div>
                            </div>
                        </div>
                    </div>
                    <div class="lc723RightDiv">
                        <div class="col l12">
                            <div id="lils382InitialLoginScreen">
                                <div class="lils382Title" style='color: #0A192F;'>Welcome</div>
                                <!-- <div class="col l12">
                                    <div class="row">
                                        <div title="Click to Sign In with your Google account" id="gmail_signin_button" class="valign-wrapper gsb183ContinueGoogleBtn" style="pointer-events: auto;">
                                            <div class="col l12 valign-wrapper gsb183GoogleButtonInner"><img class="" src="//assets-netstorage.groww.in/website-assets/prod/1.5.9/build/client/images/google-icon.5c764c55.svg" alt="Gmail Logo" width="25" height="25">
                                                <div id="gname" class="gsb183Text">
                                                    <div class="gsb183ContinueTxt">Continue with Google</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col l12 lils382DividerWrapper">
                                    <div class="lils382DividerLine"></div>
                                    <div class="lils382DividerText">Or</div>
                                    <div class="lils382DividerLine"></div>
                                </div> -->
                                <div class="col l12 lils382OtherEmail">
                                    <div id="div_modal_id">
                                        <div class="lils382EmailInput">
                                            <div class="group inf11Input"><input class="" style="font-size: 15px;" id="form_email" type="text" maxlength="250" min="0" max="10000000" autocomplete="on" placeholder="" required=""><span class="bar"></span><label class="" style="font-size: 15px;">Your Email Address Or Phone No</label></div>
                                        </div>
                                        <div class="lils382ContinueBtn">
                                            <div class="">
                                                <div class="btn51Btn btn51RipplePrimary btn51Primary" style="width: 100%; height: 50px;">
                                                    <div class="absolute-center btn51ParentDimension" onclick="checkEmail()"><span class="absolute-center" style="padding: 0px 25px;"><span>Continue</span></span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><span class="close">X</span>
        </div>
    </div>
</div>

<!-- Modal EnD -->

<style>
    .modal {
        display: none;
        /* Hidden by default */
        position: fixed;
        /* Stay in place */
        z-index: 1;
        /* Sit on top */
        left: 0;
        top: 0;
        width: 100%;
        /* Full width */
        height: 100%;
        /* Full height */
        overflow: auto;
        /* Enable scroll if needed */
        background-color: rgb(0, 0, 0);
        /* Fallback color */
        background-color: rgba(0, 0, 0, 0.4);
        /* Black w/ opacity */
    }

    /* Modal Content/Box */
    .modal-content {
        background-color: #fefefe;
        margin: 15% auto;
        /* 15% from the top and centered */
        padding: 0px;
        border: 1px solid #888;
        width: 80%;
        /* Could be more or less, depending on screen size */
    }

    /* The Close Button */
    .close {
        color: #20ca92;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }

    .close:hover,
    .close:focus {
        color: black;
        text-decoration: none;
        cursor: pointer;
    }
</style>

<script>
    // Get the modal
    var modal = document.getElementById("myModal");

    // Get the button that opens the modal
    var btn = document.getElementById("myBtn");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on the button, open the modal
    btn.onclick = function() {
        modal.style.display = "block";
    }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>


<!-- // Ajax  -->

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


<script>
    function checkEmail() {
        $email = document.getElementById('form_email').value;
        $.ajax({
            url: './request/emailCheck.php',
            type: 'POST',
            dataType: 'html',
            data: {
                email: $email,
            },
            success: function(response) {
                console.log("successful");
                $('#div_modal_id').html(response);
            },
            error: function(xhr, textStatus, errorThrown) {
                console.log("Fail");
            }

        });

    }
</script>

<script>
    function checkNID() {
        track_no = document.getElementById('form_track_no').value;
        NID = document.getElementById('form_NID').value;
        $.ajax({
            url: './request/checkNID.php',
            type: 'POST',
            dataType: 'html',
            data: {
                form_track_no: track_no,
                form_NID: NID
            },
            success: function(response) {
                console.log(response)
                if (response == 0) {
                    window.location = 'explore.php'
                } else {
                    $('#div_modal_id').html(response);
                }
            },
            error: function(xhr, textStatus, errorThrown) {
                console.log("Fail");
            }

        });

    }
</script>

<script>
    function checkOTP() {
        otp = document.getElementById('form_OTP').value;
        track_no = document.getElementById('form_track_no').value;

        console.log(otp)
        console.log(track_no)
        $.ajax({
            url: './request/confirmOTP.php',
            type: 'POST',
            dataType: 'html',
            data: {
                form_OTP: otp,
                form_track_no: track_no,
            },
            success: function(response) {
                console.log(response)
                if (response == 0) {
                    window.location = 'explore.php'
                } else {
                    $('#div_modal_id').html(response);
                }
            },
            error: function(xhr, textStatus, errorThrown) {
                console.log("Fail");
            }

        });

    }
</script>