<div style="display: none; animation-duration: 300ms;" class="rodal rodal-fade-leave" tabindex="-1">
    <div class="rodal-mask"></div>
    <div style="width: 550px; height: 240px; animation-duration: 300ms;" class="rodal-dialog rodal-zoom-leave">
        <div class="child-wrapper undefined" style="border-radius: 6px;">
            <div class="family11Account">
                <div class="fm11Box"></div>
            </div><span class="rodal-close"></span>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
<div>
    <div class="container web-align">
        <section id="goldHero">
            <div class="ph62Wrapper valign-wrapper onMount-appear-done onMount-enter-done">
                <div class="col l6">
                    <div class="row valign-wrapper ph62LeftWrapper">
                        <div>
                            <h1 class="ph62Heading">Invest in<span class="primaryClr"> Gold</span></h1>
                            <div>
                                <h4 id="heroText1" class="para ph62Para">Digital Gold Bangladesh offers Digital Gold of 99.9% purity that you can purchase, sell, and accumulate anytime.</h4>
                                <div class="">
                                    <div class="btn51Btn btn51RipplePrimary btn51Primary" style="width: 150px; height: 49px; font-size: 16px;">
                                        <div class="absolute-center btn51ParentDimension"><span class="absolute-center" style="padding: 0px 25px;"><span>Start Investing</span></span></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col l6">
                    <div class="ph62RightWrapper"><img class="ph62Img" src="./images/aaa.png" alt="Investing Made Simple - Digital Gold Bangladesh"></div>
                </div>
            </div>
        </section>
    </div>