const connectionDGBD = require('../connection/connection').connectionDGBD;
const quaries = require('../query/query');


// Promices Method

let getGoldTypeByName = async (karat_title = 0) => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.getGoldTypeByName(), [karat_title], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getGoldTypeById = async (id = 0) => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.getGoldTypeById(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getGoldTypePriceHistoryListByGold_type_ID = async (id = 0) => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.getGoldTypePriceHistoryListByGold_type_ID(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let updateGoldTypePriceHistoryByID = async (id = 0, vori_price = 0, gram_price = 0, incress_date = "1990-11-01") => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.updateGoldTypePriceHistoryByID(), [vori_price, gram_price, incress_date, id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let updateGoldPriceTypeByID = async (id = 0, vori_price = 0, gram_price = 0, update_at = "1990-11-01") => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.updateGoldPriceTypeByID(), [vori_price, gram_price, update_at, id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let addNewGoldPriceTypeHistory = async (info = {}) => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.addNewGoldPriceTypeHistory(), info, (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let addNewGoldPriceTypeHistoryAndUpdateExistingGoldPriceType = async (info = {}, type_id, todayTime) => {
    return new Promise((resolve, reject) => {
        connectionDGBD.beginTransaction(function (err) {
            if (err) { throw err; }

            connectionDGBD.query(quaries.addNewGoldPriceTypeHistory(), info, (error, result, fields) => {
                if (error) {
                    return connectionDGBD.rollback(function () {
                        resolve([]);
                    });
                }

                connectionDGBD.query(quaries.updateGoldPriceTypeByID(),[info.vori_price, info.gram_price, todayTime, type_id], function (error, results, fields) {
                    if (error) {
                        return connectionDGBD.rollback(function () {
                            resolve([]);
                        });
                    }

                    connectionDGBD.commit(function (err) {
                        if (err) {
                            return connectionDGBD.rollback(function () {
                                resolve([]);
                            });
                        }
                        resolve(results);
                    });
                });
            });
        });
    })
}




module.exports = {
    getGoldTypeByName,
    getGoldTypeById,
    getGoldTypePriceHistoryListByGold_type_ID,
    updateGoldTypePriceHistoryByID,
    updateGoldPriceTypeByID,
    addNewGoldPriceTypeHistory,
    addNewGoldPriceTypeHistoryAndUpdateExistingGoldPriceType
}

