const connectionDGBD = require('../connection/connection').connectionDGBD;
const quaries = require('../query/query');


let addNewBuyAndSaleTransaction = async (id = 0) => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.addNewBuyAndSaleTransaction(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getUserBuyAndSaleTransactionByUserId = async (id = 0) => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.getUserBuyAndSaleTransactionByUserId(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getBuyCountByUser_idGold_typeAndDateRange = async (user_id = 0, gold_type = 0, from_date = '1998-02-01', to_date = '1998-02-01') => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.getBuyCountByUser_idGold_typeAndDateRange(), [user_id, gold_type, from_date, to_date], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

// Temp 

let addNewTempBuyAndSaleTransaction = async (id = 0) => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.addNewTempBuyAndSaleTransaction(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getUserTempBuyAndSaleTransactionByUserId = async (id = 0) => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.getUserTempBuyAndSaleTransactionByUserId(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getUserTempBuyAndSaleTransactionByTrackNo = async (track_no = 0) => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.getUserTempBuyAndSaleTransactionByTrackNo(), [track_no], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

// Combine 

let addNewBuy_sellAndUpdateTemp_buy_sellAndUserGoldBalance = async (track_no = "", new_buy_sell_info = {}, new_balance = 0, karate_type = "", user_id = 0) => {
    return new Promise((resolve, reject) => {
        connectionDGBD.beginTransaction(function (err) {
            if (err) { throw err; }

            connectionDGBD.query(quaries.addNewBuyAndSaleTransaction(), new_buy_sell_info, (error, result, fields) => {
                if (error) {
                    return connectionDGBD.rollback(function () {
                        resolve([]);
                    });
                }

                connectionDGBD.query(quaries.updateStatusOfTempBuySellByTrack_No(), [track_no], function (error, results, fields) {
                    if (error) {
                        return connectionDGBD.rollback(function () {
                            resolve([]);
                        });
                    }

                    connectionDGBD.query(quaries.updateBalanceByUserIdAndKaratType(karate_type), [new_balance, user_id], function (error, results, fields) {
                        if (error) {
                            console.log(error);
                            return connectionDGBD.rollback(function () {
                                resolve([]);
                            });
                        }

                        connectionDGBD.commit(function (err) {
                            if (err) {
                                return connectionDGBD.rollback(function () {
                                    resolve([]);
                                });
                            }
                            resolve(results);
                        });

                    });

                });
            });
        });
    })
}


let addNewBuy_sellAndAddBuy_JwelleryRemoveAllCardAndUserGoldBalance = async (jwellerY_buy_data = {}, new_buy_sell_info = {}, new_balance = 0, karate_type = "", user_id = 0) => {
    return new Promise((resolve, reject) => {
        connectionDGBD.beginTransaction(function (err) {
            if (err) { throw err; }

            connectionDGBD.query(quaries.addNewBuyAndSaleTransaction(), new_buy_sell_info, (error, result, fields) => {
                if (error) {
                    return connectionDGBD.rollback(function () {
                        resolve([]);
                    });
                }

                connectionDGBD.query(quaries.addNewJwellery_buy(), jwellerY_buy_data, function (error, results, fields) {
                    if (error) {
                        console.log(error);
                        return connectionDGBD.rollback(function () {
                            resolve([]);
                        });
                    }

                    connectionDGBD.query(quaries.removeAllCartItemByUserId(), user_id , function (error, results, fields) {
                        if (error) {
                            console.log(error);
                            return connectionDGBD.rollback(function () {
                                resolve([]);
                            });
                        }

                        connectionDGBD.query(quaries.updateBalanceByUserIdAndKaratType(karate_type), [new_balance, user_id], function (error, results, fields) {
                            if (error) {
                                console.log(error);
                                return connectionDGBD.rollback(function () {
                                    resolve([]);
                                });
                            }

                            connectionDGBD.commit(function (err) {
                                if (err) {
                                    return connectionDGBD.rollback(function () {
                                        resolve([]);
                                    });
                                }
                                resolve(results);
                            });

                        });
                    });
                });
            });
        });
    })
}


module.exports = {
    getUserBuyAndSaleTransactionByUserId,
    addNewBuyAndSaleTransaction,
    addNewTempBuyAndSaleTransaction,
    getUserTempBuyAndSaleTransactionByUserId,
    getUserTempBuyAndSaleTransactionByTrackNo,
    addNewBuy_sellAndUpdateTemp_buy_sellAndUserGoldBalance,
    getBuyCountByUser_idGold_typeAndDateRange,
    addNewBuy_sellAndAddBuy_JwelleryRemoveAllCardAndUserGoldBalance
}
