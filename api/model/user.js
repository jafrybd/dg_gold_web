const connectionDGBD = require('../connection/connection').connectionDGBD;
const quaries = require('../query/query');


// Promices Method

let getUserByPhoneOrEmail = async (phone = "") => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.getUserByPhoneOrEmail(), [phone, phone], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getUserByEmail = async (email) => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.getUserByEmail(), [email], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getUserByEmailOrPhone = async (emailOrPhone) => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.getUserByEmailOrPhone(), [emailOrPhone, emailOrPhone], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getUserByNID = async (NID) => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.getUserByNID(), [NID], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let addNewUser = async (info) => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.registerUserAccount(), [info], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getUserById = async (id = 0) => { // get only active user
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.getUserById(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let updateUserStatusByID = async (status = 1, id = 0) => { // get only active user
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.updateUserStatusByID(), [status, id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}


let getUserList = async () => { // get only active user
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.getUserList(), (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}



let updateProfileByID = async (id,name) => { // get only active user
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.updateProfileByID(), [name,id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let updateUser_PasswordByID = async (id, password) => { // get only active user
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.updateUser_PasswordByID, [password, id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}





module.exports = {
    getUserByPhoneOrEmail,
    getUserByEmail,
    getUserByEmailOrPhone,
    getUserByNID,
    addNewUser,
    getUserById,
    updateUserStatusByID,
    getUserList,
    updateProfileByID,
    updateUser_PasswordByID
}

