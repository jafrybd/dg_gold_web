const connectionDGBD = require('../connection/connection').connectionDGBD;
const quaries = require('../query/query');



let getReqRequestByID = async (id = "") => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.getReqRequestByID(), [id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let addNewReqRequest= async (info = {}) => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.addNewReqRequest(), info, (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getReqRequestByTrackNo = async (tracker_no = "") => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.getReqRequestByTrackNo(), [tracker_no], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let deleteReqRequestByTrack_no = async (tracker_no = "") => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.deleteReqRequestByTrack_no(), [tracker_no], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}







module.exports = {
    getReqRequestByID,
    addNewReqRequest,
    getReqRequestByTrackNo,
    deleteReqRequestByTrack_no
}