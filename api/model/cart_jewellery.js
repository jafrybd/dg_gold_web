const connectionDGBD = require('../connection/connection').connectionDGBD;
const quaries = require('../query/query');



let getCartJwelleryListByUserId = async (user_id = 0) => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.getCartJwelleryListByUserId(), [user_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getCartJwelleryByUser_IdAndProduct_Id = async (user_id, product_id = 1) => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.getCartJwelleryByUser_IdAndProduct_Id(), [user_id, product_id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let getCartJwelleryByUser_IdAndCart_Id = async (user_id, id = 1) => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.getCartJwelleryByUser_IdAndCart_Id(), [user_id, id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let addNewCartJwellery = async (info = {}) => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.addNewCartJwellery(), info, (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let updateCartJwelleryQuentityById = async (id = 0, new_qty = 1) => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.updateCartJwelleryQuentityById(), [new_qty, id], (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

let removeCartJwelleryById = async (id = 0) => {
    return new Promise((resolve, reject) => {
        connectionDGBD.query(quaries.removeCartJwelleryById(), id, (error, result, fields) => {
            if (error) reject(error)
            else resolve(result)
        });
    });
}

module.exports = {
    getCartJwelleryListByUserId,
    getCartJwelleryByUser_IdAndProduct_Id,
    updateCartJwelleryQuentityById,
    addNewCartJwellery,
    getCartJwelleryByUser_IdAndCart_Id,
    removeCartJwelleryById
}