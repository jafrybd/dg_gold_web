const express = require("express");
const router = express.Router();
const log_reg_route = require('./log_reg');
const transaction_route = require('./transaction');
const product_route = require('./product');
const gold_route = require('./gold');
const jewellery_cart_route = require('./jewellery_cart');

router.use('/auth', log_reg_route);
router.use('/transaction', transaction_route);
router.use('/product', product_route);
router.use('/gold', gold_route);
router.use('/jewellery-cart', jewellery_cart_route);

router.get('/get', (req, res) => {
    console.log(req.query);
    return res.send({
        "success": true,
        "message": "",
        "api v": 1
    });
});

router.get('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "success": true
    })
});


router.post('/*', (req, res) => {
    return res.send({
        'status': 400,
        'message': "unknown route",
        "success": true
    })
});



module.exports = router;